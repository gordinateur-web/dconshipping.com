<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
   <title>Logistics Services | Import Export Freight | DCON Shipping</title>
   <meta name="keywords" content="Export Freight Services, Air Freight Services, Import Freight Services, Freight Logistics Companies,Warehouse Services, Air Cargo Services, Custom Clearance, Transportation Service, Project Logistics Service">
   <meta  name="description" content="Global logistics services that you can trust.">
</head>
<body>
   <?php include("header.php"); ?>
   <div class="main">
   	<!-- <-----------breadcum ----------->
   <section class="breadcum">
      <img src="images/services-breadcum.jpg" class="img-fluid w-100" alt="services-breadcum">
      <div class="container">
         <div class="caption">
            <h3 class="f-bold white">Dcon<br> Shipping</h3>
            <p class="f-regular white">shipping companies worldwide thereby showcasing  the Indian excellence.</p>
         </div>
      </div>
   </section>
   <!-- <-----------breadcum ----------->

   	<!---------- services  --------->
   	<section class="services-tabs">
   		<div class="container">
   			<div class="row">
   				<div class="d-none d-md-block col-md-4">
   					<h4 class="f-semi-bold black" data-aos="fade-up">Our Services</h4>
   					<ul class="nav nav-tabs" role="tablist" data-aos="fade-up" data-aos-delay="100">
   						<li class="nav-item">
   							<a class="nav-link active" data-toggle="tab" href="#freight-forwarding" role="tab" aria-controls="freight-forwarding" aria-selected="true"> <span class="icon-cruise-1 style-icon"></span>
   							Freight Forwarding (AIR & SEA)</a>
   						</li>
   						<li class="nav-item">
   							<a class="nav-link" data-toggle="tab" href="#custom-clearance" role="tab" aria-controls="profile" aria-selected="false">
   							<span class="icon-533984 style-icon"></span>
   							Custom Clearance (AIR & SEA)</a>
   						</li>
   						<li class="nav-item">
   							<a class="nav-link" data-toggle="tab" href="#transportation" role="tab" aria-controls="contact" aria-selected="false">
   							<span class="icon-sfgb67 style-icon"></span>
   							Transportation</a>
   						</li>
   						<li class="nav-item">
   							<a class="nav-link" data-toggle="tab" href="#warehouse" role="tab" aria-controls="contact" aria-selected="false">
   							<span class="icon-warehouse-1 style-icon"></span>
   							Warehousing</a>
   						</li>
   						<li class="nav-item">
   							<a class="nav-link" data-toggle="tab" href="#project-logistics" role="tab" aria-controls="contact" aria-selected="false">
   							<span class="icon-sea-ship-with-containers style-icon"></span>
   							Project Logistics</a>
   						</li>
   						<li class="nav-item">
   							<a class="nav-link" data-toggle="tab" href="#value-added-services" role="tab" aria-controls="contact" aria-selected="false">
   							<span class="icon-Value-Added-Services style-icon"></span>
   							Value Added Services</a>
   						</li>
   					</ul>
   				</div>
   				<div class="col-md-8" data-aos="fade-up" data-aos-delay="200">
   					<div class="tab-content">
   						<div class="tab-pane show active" id="freight-forwarding" role="tabpanel" aria-labelledby="freight-forwarding-tab">
   							<div class="card">
   								<div class="card-header">
   									<h2 class="mb-0">
   										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#freight-forwarding-tab" aria-expanded="true" aria-controls="freight-forwarding-tab">
   											<span class="icon-cruise-1 style-icon"></span> Freight Forwarding (AIR & SEA)
   										</button>
   									</h2>
   								</div>

   								<div id="freight-forwarding-tab" class="collapse show" data-parent=".tab-content">
   									<div class="card-body">
   										<h3 class="f-bold black">Freight Forwarding (AIR & SEA)</h3>
   										<span class="icon-cruise-1 style-icon"></span>

                                 <p class="f-regular">DECON is a perfect choice for all your cargo transportation solutions. We provide you an extensive range of services to our customers that includes door to door cargo services & airport to airport cargo services. Whereas, with our best infrastructure facility and large overseas network, ensures smooth & hassle-free transportation of goods.</p>

                                 <p class="f-regular">DECON is one of the most efficient international sea freight service providers in India offering our clients a comprehensive range of cargo shipment services. We have a set of dedicated & well-trained professionals who are well familiar with all the import/export, freight forwarding (Air & Sea), and customs clearance of complex international cargo shipments. With the help of our vast network of reliable agents worldwide, we ensure the safe delivery of all the goods.</p>

                                 <div class="img-box">
                                    <img src="images/freight-forwarding-big.jpg" class="img-fluid w-100" alt="freight-forwarding-big">
                                 </div>

   									</div>
   								</div>
   							</div>
   						</div>
   						<div class="tab-pane collapse" id="custom-clearance" role="tabpanel" aria-labelledby="profile-tab">
   							<div class="card">
   								<div class="card-header">
   									<h2 class="mb-0">
   										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#custom-clearance-tab" aria-expanded="false" aria-controls="custom-clearance-tab">
   										<span class="icon-533984 style-icon"></span> custom-clearance
   										</button>
   									</h2>
   								</div>

   								<div id="custom-clearance-tab" class="collapse" data-parent=".tab-content">
   									<div class="card-body">
   										<h3 class="f-bold black">Custom Clearance (AIR & SEA)</h3>
   										<span class="icon-533984 style-icon"></span>
   										<p class="f-regular">DCON Shipping Customs Clearing Agents ensure smooth and easy customs clearance of all the customers so that they receive their goods on time. Whereas, Our Customs Brokers helps in easy Import and Export regulations and paperwork in a very small time for all the shipments. As we all know every country has its own laws for custom clearance so our qualified custom agents provide reliability to the Customs Clearing service. By handling all the trade compliance and procedures, we help to clear consignments by sea, land, and air more efficiently.</p>

                                 <div class="img-box">
                                    <img src="images/custom-clearance-big.jpg" class="img-fluid w-100" alt="freight-forwarding-big">
                                 </div>
   									</div>
   								</div>
   							</div>
   						</div>
   						<div class="tab-pane collapse" id="transportation" role="tabpanel" aria-labelledby="transportation-tab">
   							<div class="card">
   								<div class="card-header">
   									<h2 class="mb-0">
   										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#transportation-tab" aria-expanded="false" aria-controls="transportation-tab">
   										<span class="icon-sfgb67 style-icon"></span> Transportation
   										</button>
   									</h2>
   								</div>

   								<div id="transportation-tab" class="collapse" data-parent=".tab-content">
   									<div class="card-body">
   										<h3 class="f-bold black">Transportation</h3>
   										<span class="icon-sfgb67 style-icon"></span>
   										<p class="f-regular">DCON provides its customers with an excellent domestic & international land transport shipping. Regardless of any destination, we make arrangements for all transport facilities that will suit your cargo requirements. Whereas, we have a team of professionals who are experts in handling all the packing, warehousing & transportation of goods. Our company assures safe & smooth delivery of your materials at the said destination on time. Also, we have lots of heavy-duty trailers, refrigerated trucks, and trucks for oil gas, that enables you to transport any kind of cargo.</p>

                                 <div class="img-box">
                                    <img src="images/transportation-big.jpg" class="img-fluid w-100" alt="freight-forwarding-big">
                                 </div>
   									</div>
   								</div>
   							</div>
   						</div>
   						<div class="tab-pane collapse" id="warehouse" role="tabpanel" aria-labelledby="warehouse-tab">
   							<div class="card">
   								<div class="card-header">
   									<h2 class="mb-0">
   										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#warehouse-tab" aria-expanded="false" aria-controls="warehouse-tab">
   										<span class="icon-sfgb67 style-icon"></span> Warehouse
   										</button>
   									</h2>
   								</div>

   								<div id="warehouse-tab" class="collapse" data-parent=".tab-content">
   									<div class="card-body">
   										<h3 class="f-bold black">Warehouse</h3>
   										<span class="icon-sfgb67 style-icon"></span>
   										<p class="f-regular">Our warehousing facilities provide the customers a professional warehousing services and care ensures the safe arrival of your product anywhere, anytime. These warehouses are selected after examining their suitability with certain factors like protection of goods from loss, shortage, theft, and damage. Along with these, our aim is to fulfill the needs of the customers we also facilitate the process of redistribution to the final destination. Whereas we have vast experience of the industry in offering warehousing services to all our clients. In order to fulfill the requirement of the clients, we have associated ourselves with various warehouses that are close to the port of loading and unloading.</p>

                                 <div class="img-box">
                                    <img src="images/warehouse-big.jpg" class="img-fluid w-100" alt="freight-forwarding-big">
                                 </div>
   									</div>
   								</div>
   							</div>
   						</div>
   						<div class="tab-pane collapse" id="project-logistics" role="tabpanel" aria-labelledby="project-logistics-tab">
   							<div class="card">
   								<div class="card-header">
   									<h2 class="mb-0">
   										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#project-logistics-tab" aria-expanded="false" aria-controls="project-logistics-tab">
   										<span class="icon-sea-ship-with-containers style-icon"></span> Project Logistics
   										</button>
   									</h2>
   								</div>

   								<div id="project-logistics-tab" class="collapse" data-parent=".tab-content">
   									<div class="card-body">
   										<h3 class="f-bold black">Project Logistics</h3>
   										<span class="icon-sea-ship-with-containers style-icon"></span>
   										<p class="f-regular">DCON shipping’s project Logistics is a specialized field of freight forwarding and shipping business. It requires immense expertise, experience, and knowledge. We at DCON Shipping are fully equipped to handle all types of heavy lift and over-sized goods. Our highly experienced teams of experts have been specializing in comprehensive logistics solutions for plant, machinery, and equipment. We provide one-stop job control for all goods flow.</p>

                                 <div class="img-box">
                                    <img src="images/project-logistics-big.jpg" class="img-fluid w-100" alt="freight-forwarding-big">
                                 </div>
   									</div>
   								</div>
   							</div>
   						</div>
   						<div class="tab-pane collapse" id="value-added-services" role="tabpanel" aria-labelledby="value-added-services-tab">
   							<div class="card">
   								<div class="card-header">
   									<h2 class="mb-0">
   										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#value-added-services-tab" aria-expanded="false" aria-controls="value-added-services-tab">
   										<span class="icon-sfgb67 style-icon"></span> Value Added Services
   										</button>
   									</h2>
   								</div>

   								<div id="value-added-services-tab" class="collapse" data-parent=".tab-content">
   									<div class="card-body">
   										<h3 class="f-bold black">Value Added Services</h3>
   										<span class="icon-sfgb67 style-icon"></span>
   										<p class="f-regular">We provide a large number of value-added services that can improve the efficiency and responsiveness of your shipping chain even more further. By understanding the issues and anticipating the logistics needs, our experts provide a robust solution that will drive value for the shipping business. Whereas, DCON shipping provides door-to-door delivery service for customers who want to make the logistic management simple, reduce cost, and minimize the risk of loss or damage. By holding the strength and reliability of the global network, DCON reduces days of supply chain and delivers goods directly to the supplier to your desired destinations.</p>

                                 <div class="img-box">
                                    <img src="images/value-added-services-big.jpg" class="img-fluid w-100" alt="freight-forwarding-big">
                                 </div>
   									</div>
   								</div>
   							</div>
   						</div>
   					</div>
   				</div>
   			</div>
   		</div>
   	</section>
   	<!---------- services  --------->

   	<?php include("footer.php"); ?>

      <script type="text/javascript">

        
         $('a[data-toggle="tab"]').on('click', function (e) {
            var ele_top=$('.tab-content');
            $("html, body").animate({ scrollTop: $(ele_top).offset().top - ($('header').height() + 50) }, 800);
         }); 

// tab pane scrolling top
var hash=window.location.hash;
    var ele=$('.nav-tabs a[href="'+hash+'"][data-toggle="tab"]');
    var ele_top=$('.tab-content');
    if(ele.length > 0) {
        $(ele).trigger('click');
        $("html, body").animate({ scrollTop: $(ele_top).offset().top - ($('header').height() +100) }, 800);
    }
    if($('.tab-content .collapse').hasClass('show')) {
        $('.tab-content .collapse').removeClass('show');
    }
    $('.btn.btn-link').attr('aria-expanded','false');
    $(hash+' button').attr('aria-expanded','true');
    $(hash+'-tab').addClass('show');

    // only for same page scroll 

    // $(window).on( 'hashchange', function(e) {
    // var hash=window.location.hash;
    // var ele=$('.nav-tabs a[href="'+hash+'"][data-toggle="tab"]');
    // var ele_top=$('.tab-content');
    // if(ele.length > 0) {
    //     $(ele).trigger('click');
    //     $("html, body").animate({ scrollTop: $(ele_top).offset().top - ($('header').height() ) }, 800);
    // }
    // if($('.tab-content .collapse').hasClass('show')) {
    //     $('.tab-content .collapse').removeClass('show');
    // }
    // $('.btn.btn-link').attr('aria-expanded','false');
    // $(hash+' button').attr('aria-expanded','true');
    // $(hash+'-tab').addClass('show');
    // });
    
$(document).ready(function(){

    $('.card-header .btn-link').on('click',function(){
        var ele= this;
        setTimeout(function() {
            if(!in_viewport(ele)) {
                $('html,body').animate({
                    scrollTop: $(ele).offset().top - $('header').height() - 50
                },1000);
            }
        },1000);
    }); 

    function in_viewport(ele) {
        var top_of_element = $(ele).offset().top;
        var bottom_of_element = $(ele).offset().top + $(ele).outerHeight();
        var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
        var top_of_screen = $(window).scrollTop();

        if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
            return true
        }
        return false;
    }

});

// $('a[data-toggle="tab"]').on('click', function (e) {
//     var ele_top=$('.tab-content');
//     $("html, body").animate({ scrollTop: $(ele_top).offset().top - ($('.breadcum').height() - 200) }, 800);
// });  

$(window).on( 'hashchange', function(e) {
    var hash=window.location.hash;
    var ele=$('.nav-tabs a[href="'+hash+'"][data-toggle="tab"]');
    var ele_top=$('.tab-content');
    if(ele.length > 0) {
        $(ele).trigger('click');
        $("html, body").animate({ scrollTop: $(ele_top).offset().top - ($('.breadcum').height() - 200) }, 800);
    }
    if($('.tab-content .collapse').hasClass('show')) {
        $('.tab-content .collapse').removeClass('show');
    }
    $('.btn.btn-link').attr('aria-expanded','false');
    $(hash+' button').attr('aria-expanded','true');
    $(hash+'-tab').addClass('show');
});

    </script>
   </div>
</body>
</html>
