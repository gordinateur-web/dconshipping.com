<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
   <title>About | Logistics Management Company | DCON Shipping</title>
   <meta name="keywords" content="Logistics Management Company, Warehouse Services, Logistics Solutions, Import And Export Solution, Cost Effective Logistics.">
   <meta  name="description" content="DCON Shipping has emerged as one of the leading International freight, warehousing and logistics management companies in India.">
</head>
<body>
   <?php include("header.php"); ?>
   <div class="main">
   <!-- <-----------breadcum ----------->
   <section class="breadcum">
      <img src="images/about-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container">
         <div class="caption">
            <h3 class="f-bold white">Dcon<br> Shipping</h3>
            <p class="f-regular white">International Freight Forwarder Company Located In India.</p>
         </div>
      </div>
   </section>
   <!-- <-----------breadcum ----------->

   <!-- <-----------company ----------->
   <section class="company">
      <div class="container">
         <div class="row">
            <div class="col-md-6">
               <div class="img-box" data-aos="fade-up">
                  <img src="images/company-overview.jpg" class="img-fluid w-100" alt="company-overview">
               </div>
            </div>
            <div class="col-md-6">
               <div class="text-box">
                  <h2 class="f-bold black title" data-aos="fade-up" data-aos-delay="100">Company Overview</h2>
                  <p class="f-light" data-aos="fade-up" data-aos-delay="100">DCON Shipping is an International Freight Forwarder logistics company located in India. We provide our customers a hassle-free services such as transportation, air, and sea freight forwarding, custom clearing activities, warehousing storage services, project Logistics, and value-added services. Also, in addition, they manage port handling, loading and unloading services, throughout the country. We can provide first-class service with competitive rates to all the destinations worldwide with our strong international agency network around the globe. Our independent overseas representatives and partners are appointed after the careful screening to ensure our policy standards are not compromised.</p>

                  <p class="f-light" data-aos="fade-up" data-aos-delay="200">A team of an enthusiastic and talented professional staff of DCON Shipping helped us to become one of the trusted and recognized names in the freight forwarding field. No matter small or large volumes we handle all cargo with the utmost care and offer the finest personalized attention and services.</p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- <-----------company ----------->

   <!-- <-----------vision-mission ----------->
   <section class="vision-mission">
      <div class="container">
         <div class="row">
            <div class="col-md-4 col-lg-3 offset-lg-1">
               <div class="nav-wrapp" data-aos="fade-up" data-aos-delay="100">
                  <ul class="nav nav-tabs" role="tablist">
                   <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#our-vision" role="tab" aria-controls="home" aria-selected="true"><span class="icon-vision style-icon"></span>OUR VISION</a>
                 </li>
                 <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#our-mission" role="tab" aria-controls="profile" aria-selected="false"><span class="icon-download-1 style-icon"></span>OUR MISSION</a>
                 </li>
                 <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#our-value" role="tab" aria-controls="contact" aria-selected="false"><span class="icon-download-3 style-icon"></span>Our Values</a>
                 </li>
              </ul>
           </div>
            </div>

            <div class="col-md-8 col-lg-6 offset-lg-1" data-aos="fade-up" data-aos-delay="200">
               <div class="tab-content">
                  <div class="tab-pane fade show active" id="our-vision" role="tabpanel" aria-labelledby="vision-tab"><p class="f-regular white">To create a Professional atmosphere and to develop Adaptability that enables us to face and resolve them without compromising the quality of our service to build long-term strategic relationships with our customers.</p>
                </div>
                <div class="tab-pane fade" id="our-mission" role="tabpanel" aria-labelledby="mission-tab">
                   <p class="f-regular white">Our mission is to provide value-added, innovative, reliable & cost-effective logistic services to our customers with state-of-the-art infrastructure and resources.</p>
               </div>
                <div class="tab-pane fade" id="our-value" role="tabpanel" aria-labelledby="value-tab">
                      <p class="f-regular white">DCON Shipping takes very serious responsibility for safety in all aspects of our business and operations. Also, we are constantly improving our services by developing new ideas and adopting creative approaches to handle challenging projects.</p>
                </div>
             </div>
            </div>
         </div>
      </div>
   </section>
   <!-- <-----------vision-mission ----------->

   <!-- <-----------our hightlights ----------->
   <section class="hightlights">
      <h2 class="f-bold black title text-center" data-aos="fade-up">Our Highlights</h2>
      <div class="container flex-box">
         <div class="item" data-aos="fade-up" data-aos-delay="100">
            <span class="style-icon icon-download-4-1"></span>
            <p class="f-semi-bold grey">Quality Guarantee</p>
         </div>
         <div class="item" data-aos="fade-up" data-aos-delay="200">
            <span class="style-icon icon-download-5"></span>
            <p class="f-semi-bold grey">Convenience & Speed</p>
         </div>
         <div class="item" data-aos="fade-up" data-aos-delay="300">
            <span class="style-icon icon-download-6"></span>
            <p class="f-semi-bold grey">Reliability</p>
         </div>
         <div class="item" data-aos="fade-up" data-aos-delay="400">
            <span class="style-icon icon-download-7"></span>
            <p class="f-semi-bold grey">Efficiency</p>
         </div>
      </div>
   </section>
   <!-- <-----------our hightlights ----------->



   </div>

         <?php include("footer.php"); ?>

         <script type="text/javascript">
         </script>
</body>
</html> 
