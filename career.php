<?php 
    require_once("config.php"); 
    $auth=$auth_array;
    $auth['webform']='dconshipping-career';
    $token= get_token($auth);
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
   <title>Career | Logistics Company | DCON Shipping</title>
   <meta name="keywords" content="Logistics Company India, Import And Export, Transportation Services, Custom Clearance">
   <meta  name="description" content="DCON shipping believes in hiring quality manpower, who will take the group to an overwhelming heights of achievement through a strong focus on their career and growth.">
</head>
<body>
   <?php include("header.php"); ?>
   <div class="main">
   <!-- <-----------breadcum ----------->
   <section class="breadcum">
      <img src="images/career-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container">
         <div class="caption">
            <h3 class="f-bold white">Dcon<br> Shipping</h3>
            <p class="f-regular white">DCON Shipping is a people-oriented organization.</p>
         </div>
      </div>
   </section>
   <!-- <-----------breadcum ----------->

  <!-- career  -->
  <section class="career">
    <h2 class="title f-bold black text-center" data-aos="fade-up">Career & Job Opportunities</h2>
    <div class="container">
      <div class="wrapp" data-aos="fade-up" data-aos-delay="100">
        <div class="item left-div">
          <h4 class="">For future career growth and achievements, you can join us. Just fill in the below form to contact us.</h4>

            <img src="images/career-ship.png" class="img-fluid m-auto d-block" alt="career-ship">
        </div>
        <div class="item right-div">
          <h3 class="black f-bold">Contact Form |</h3>
          <span class="require">Required fields are marked with *</span>
          <form action="form_send.php" class="form-validation" method="post" enctype="multipart/form-data">
          <input type="hidden" name="token" value="<?php echo $token; ?>">
            <div class="form-group">
              <label class="f-medium black">Name<sup>*</sup></label>
              <input name="text" type="text" class="form-control input-style" placeholder="Your Name" data-validation="required">
            </div>
            <div class="form-group">
              <label class="f-medium black">Email<sup>*</sup></label>
              <input name="email" type="email" class="form-control input-style" placeholder="Email" data-validation="required">
            </div>
            <div class="form-group">
              <label class="f-medium black">Subject<sup>*</sup></label>
              <input name="subject" type="text" class="form-control input-style" placeholder="Subject" data-validation="required">
            </div>
            <div class="form-group">
              <label class="f-medium black">Message<sup>*</sup></label>
              <textarea name="message" class="form-control input-style" rows="3" placeholder="Message" data-validation="required"></textarea>
            </div>
            <div class="button-box">
              <button type="reset" class="btns">Clear</button>
              <button type="submit" class="btns">submit</button>
            </div>
          </form>
        </div> 
      </div>
      <div class="red-strip">
        <div class="left-div">
          <h3 class="white f-regular">Join us on board for the career journey of your life.<br> Contact or Mail on</h3>
        </div>
        <div class="right-div">
          <p class="f-medium">Phone: <a href="tel:+911234567899">+91 1234567899</a></p>
          <p class="f-medium">Email: <a href="emailto:info@dconshippig.com">info@dconshippig.com</a></p>
        </div>
      </div>
    </div>
  </section>
  <!-- career  -->

   </div>
  <?php include("footer.php"); ?>

  <?php include("show_msg.php"); ?>

  <script src="https://www.google.com/recaptcha/api.js"></script>
  <script type="text/javascript" src="js/jquery.form-validator.min.js"></script>

   <script type="text/javascript">

    $.validate({
      form: ".form-validation",
    });

   </script>
</body>
</html> 
