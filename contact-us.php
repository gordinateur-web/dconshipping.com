<?php 
    require_once("config.php"); 
    $auth=$auth_array;
    $auth['webform']='dconshipping-contact';
    $token= get_token($auth);
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
   <title>Contact Us | Logistics Management | DCON Shipping</title>
   <meta name="keywords" content="DCON Shipping, Best Logistics Company, Logistics Management Company, Import & Export Consultancy, Warehousing,
">
   <meta  name="description" content="DCON shipping believes in hiring quality manpower, who will take the group to an overwhelming heights of achievement through a strong focus on their career and growth.">
</head>
<body>
   <?php include("header.php"); ?>
   <div class="main">
   <!-- <-----------breadcum ----------->
   <section class="breadcum">
      <img src="images/contact-breadcum.jpg" class="img-fluid w-100" alt="contact-breadcum">
      <div class="container">
         <div class="caption">
            <h3 class="f-bold white">Dcon<br> Shipping</h3>
            <p class="f-regular white">We Are Here To Provide Shipping Solutions, Communicate With Us Anytime</p>
         </div>
      </div>
   </section>
   <!-- <-----------breadcum ----------->

  <!-- contact  -->
  <section class="contact career">
    <h2 class="title f-bold black text-center" data-aos="fade-up">Get In Touch With Us</h2>
    <h6 class="grey f-medium text-center" data-aos="fade-up" data-aos-delay="100">You can talk to our representative anytime or else fill the form below for an instant messaging program.</h6>
    <div class="container">
      <div class="row">
        <div class="col-md-6" data-aos="fade-up" data-aos-delay="100">
          <form action="form_send.php" class="form-validation" method="post" enctype="multipart/form-data">
          <input type="hidden" name="token" value="<?php echo $token; ?>">
            <div class="form-group">
              <label class="f-medium black">Name<sup>*</sup></label>
              <input name="text" type="text" class="form-control input-style" placeholder="Your Name" data-validation="required">
            </div>
            <div class="form-group">
              <label class="f-medium black">Email<sup>*</sup></label>
              <input name="email" type="email" class="form-control input-style" placeholder="Email" data-validation="required">
            </div>
            <div class="form-group">
              <label class="f-medium black">Phone No.<sup>*</sup></label>
              <input name="phone" type="number" class="form-control input-style" placeholder="Subject" data-validation="required">
            </div>
            <div class="form-group">
              <label class="f-medium black">Message<sup>*</sup></label>
              <textarea name="message" class="form-control input-style" rows="3" placeholder="Message" data-validation="required"></textarea>
            </div>
            <div class="button-box">
              <button type="reset" class="btns">Clear</button>
              <button type="submit" class="btns">submit</button>
            </div>
          </form>
        </div>
        <div class="col-md-6" data-aos="fade-up" data-aos-delay="200">
          <div class="right-box">
            <div class="cols">
              <span class="icon-Pointer1 style-icon"></span>
              <div class="text-box">
                <p class="f-semi-bold black">Kukreja, Sector- 1, Khanda Colony,<br> Panvel, Navi Mumbai- 410 206</p>
              </div>
            </div>
            <div class="cols">
              <span class="icon-Plaine1 style-icon"></span>
              <div class="text-box">
                <a href="mailto:info@dconshippig.com" class="f-semi-bold black">info@dconshippig.com</a>
                <a href="mailto:abc@dconshippig.com" class="f-semi-bold black">abc@dconshippig.com</a>
              </div>
            </div>
            <div class="cols">
              <span class="icon-Phone21 style-icon"></span>
              <div class="text-box">
                <a href="tel:+911234567899" class="f-semi-bold black">+91-1234567899</a>
                <a href="tel:+911234567899" class="f-semi-bold black">+91-1234567899</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- contact  -->

  <section class="map-box">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.0522858008485!2d73.04126761531263!3d19.017417558752346!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c252fff95555%3A0xd3c6b58b2c69a7cb!2sG-Ordinateur%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1582023698666!5m2!1sen!2sin" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
  </section>

   </div>
  <?php include("footer.php"); ?>

  <?php include("show_msg.php"); ?>

  <script src="https://www.google.com/recaptcha/api.js"></script>
  <script type="text/javascript" src="js/jquery.form-validator.min.js"></script>

   <script type="text/javascript">

    $.validate({
      form: ".form-validation",
    });

   </script>
</body>
</html> 
