<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
   <title>Home | Warehousing & Transportation Services | DCON Shipping </title>
   <meta name="keywords" content="DCON Shipping, Shipping Service Provider of India, Logistic Company,  Warehousing, Transportation Services, Import Export Company Custom Clearance, Freight Forwarding, International Logistics Companies.">
   <meta name="description" content="DCON Shipping Believes in providing logistic services from a professional perspective by focusing on fulfilling expectations of the customer's logistics needs.">
</head>
<body>
   <?php include("header.php"); ?>
   <div class="main">
      <!-- <-----------home slider ----------->
         <section class="home-slider breadcum">
            <div id="home-slider" class="carousel slide" data-ride="carousel">
               <ol class="carousel-indicators">
                  <li data-target="#home-slider" data-slide-to="0" class="active"></li>
                  <li data-target="#home-slider" data-slide-to="1"></li>
                  <li data-target="#home-slider" data-slide-to="2"></li>
               </ol>
               <div class="carousel-inner">
                  <div class="carousel-item active">
                     <img src="images/slider1.jpg" class="d-block w-100" alt="images/slider1.jpg">
                     <div class="container caption-box">
                        <h2 class="animated fadeInDown white f-bold">Providing Logistics Solutions Globally</h2>
                        <h5 class="white f-medium animated fadeInDown">A Pioneer in shipping, clearing, and forwarding of goods all over the Globe</h5>
                        <a href="about-us.php" class="btns f-ex-bold animated fadeInDown">Know More <span class="icon-right-arrow11"></span></a>
                     </div> 
                  </div>
                  <div class="carousel-item">
                     <img src="images/slider2.jpg" class="d-block w-100" alt="images/slider1.jpg">
                     <div class="container caption-box">
                        <h2 class="animated fadeInDown white f-bold">Providing Logistics Solutions Globally</h2>
                        <h5 class="white f-medium animated fadeInDown">A Pioneer in shipping, clearing, and forwarding of goods all over the Globe</h5>
                        <a href="about-us.php" class="btns f-ex-bold animated fadeInDown">Know More <span class="icon-right-arrow11"></span></a>
                     </div> 
                  </div>
                  <div class="carousel-item">
                     <img src="images/slider3.jpg" class="d-block w-100" alt="images/slider1.jpg">
                     <div class="container caption-box">
                        <h2 class="animated fadeInDown white f-bold">Providing Logistics Solutions Globally</h2>
                        <h5 class="white f-medium animated fadeInDown">A Pioneer in shipping, clearing, and forwarding of goods all over the Globe</h5>
                        <a href="about-us.php" class="btns f-ex-bold animated fadeInDown">Know More <span class="icon-right-arrow11"></span></a>
                     </div>  
                  </div>
                  <a class="carousel-control-prev" href="#home-slider" role="button" data-slide="prev">
                     <span class="icon-left-arrow1"></span>
                  </a>
                  <a class="carousel-control-next" href="#home-slider" role="button" data-slide="next">
                     <span class="icon-right-arrow11"></span>
                  </a>
               </div>
            </div>
         </section>
         <!-- <-----------home slider ----------->
         
         <!-- <-----------services ----------->
            <section class="services">
               <div class="container">
                  <div class="owl-carousel owl-theme services-carousel">
                     <div class="item">
                        <a href="services.php#freight-forwarding">
                           <div class="caption">
                              <h5 class="f-medium black">FREIGHT FORWARDING </h5>
                              <span class="icon-cruise-1 style-icon"></span>
                              <p class="f-light">At Dcon Ship Management, we have an extensive range of solutions..</p>
                           </div>
                           <div class="img-box">
                              <img src="images/forwording-thumb.jpg" class="img-fluid" alt="forwording-thumb">
                              <span class="icon-back-arrow arrows"></span>
                           </div>
                        </a>
                     </div>
                     <div class="item">
                        <a href="services.php#custom-clearance">
                           <div class="caption">
                              <h5 class="f-medium black">Custom Clearance </h5>
                              <span class="icon-533984 style-icon"></span>
                              <p class="f-light">At Dcon Ship Management, we have an extensive range of solutions..</p>
                           </div>
                           <div class="img-box">
                              <img src="images/clearance-thumb.jpg" class="img-fluid" alt="clearance-thumb">
                              <span class="icon-back-arrow arrows"></span>
                           </div>
                        </a>
                     </div>
                     <div class="item">
                        <a href="services.php#transportation">
                           <div class="caption">
                              <h5 class="f-medium black">TRASPORTATION</h5>
                              <span class="icon-sfgb67 style-icon"></span>
                              <p class="f-light">At Dcon Ship Management, we have an extensive range of solutions..</p>
                           </div>
                           <div class="img-box">
                              <img src="images/transporataion-thumb.jpg" class="img-fluid" alt="transportation-thumb">
                              <span class="icon-back-arrow arrows"></span>
                           </div>
                        </a>
                     </div>
                     <div class="item">
                        <a href="services.php#warehouse">
                           <div class="caption">
                              <h5 class="f-medium black">WAREHOUSE</h5>
                              <span class="icon-warehouse-1 style-icon"></span>
                              <p class="f-light">At Dcon Ship Management, we have an extensive range of solutions..</p>
                           </div>
                           <div class="img-box">
                              <img src="images/warehouse-thumb.jpg" class="img-fluid" alt="warehouse-thumb">
                              <span class="icon-back-arrow arrows"></span>
                           </div>
                        </a>
                     </div>
                     <div class="item">
                        <a href="services.php#project-logistics">
                           <div class="caption">
                              <h5 class="f-medium black">Project Logistics</h5>
                              <span class="icon-sea-ship-with-containers style-icon"></span>
                              <p class="f-light">At Dcon Ship Management, we have an extensive range of solutions..</p>
                           </div>
                           <div class="img-box">
                              <img src="images/project-logistics-thumb.jpg" class="img-fluid" alt="warehouse-thumb">
                              <span class="icon-back-arrow arrows"></span>
                           </div>
                        </a>
                     </div>
                     <div class="item">
                        <a href="services.php#value-added-services">
                           <div class="caption">
                              <h5 class="f-medium black">Value Added Services</h5>
                              <span class="icon-Value-Added-Services style-icon"></span>
                              <p class="f-light">At Dcon Ship Management, we have an extensive range of solutions..</p>
                           </div>
                           <div class="img-box">
                              <img src="images/value-added-services-thumb.jpg" class="img-fluid" alt="warehouse-thumb">
                              <span class="icon-back-arrow arrows"></span>
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
            </section>
         <!-- <-----------services ----------->

         <section class="doc-shipping">
            <div class="container">
               <h2 class="f-medium black text-center" data-aos="fade-up">Dcon Shipping Agency Profile</h2>
               <div class="wrapp w-100" data-aos="fade-up" data-aos-delay="100">
                  <div class="item">
                     <div class="text-box">
                        <h5 class="f-medium">About Dcon Agency</h5>
                        <h3 class="f-bold black">Who We are</h3>
                        <p class="f-light black">Dcon Ship Management is a reliable partner in Ship management and international manning services. We are organized company formed by the pool of shipping management directors and incorporators whose expertise is among the best in the labour relations, human resource, maritime laws, mass communications training and development.</p>
                        <a href="about-us.php" class="btns f-bold">Explore More <span class="icon-right-arrow11"></span></a>
                     </div>
                  </div>
                  <div class="item">
                     <div class="img-box">
                        <img src="images/ship-agency.jpg" class="img-fluid w-100" alt="ship-agency">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         </div>

         <?php include("footer.php"); ?>

         <script type="text/javascript" src="js/owl.carousel.min.js"></script>
         <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
         <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">

         <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.js"></script>

         <script type="text/javascript">

            $(document).ready(function(){
               $('.owl-carousel').owlCarousel({
                 loop:false,
                 margin:0,
                 dots:false,
                 nav:true,
                 navText:['<span class="icon-left-arrow1"></span>','<span class="icon-right-arrow11"></span>'],
                 responsive:{
                  0:{
                     items:1,
                  },
                  576:{
                     items:2,
                  },
                  767:{
                     items:2,
                  },
                  992:{
                     items:3,
                  },
                  1200:{
                     items:4,
                  }
               }
            });
            });
               // ondragg slide item 
         $('.carousel').carousel({
            interval: false,
         })
         $(".carousel").swipe({
            swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
               if (direction == 'left') $(this).carousel('next');
               if (direction == 'right') $(this).carousel('prev');
            },
            allowPageScroll:"vertical"
         });
</script>
</body>
</html> 
