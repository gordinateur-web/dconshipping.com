<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
   <title>404 Error | Dcon Shipping</title>
   <meta content="" name="description">
   <meta content="" name="author">
</head>
<body>
   <?php include("header.php"); ?>
   <div class="main">
   <!-- <-----------breadcum ----------->
     <section class="breadcum line">
      <img src="images/career-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container">
         <div class="caption">
            <h3 class="f-bold white">404 Error</h3>
         </div>
      </div>
   </section>
   <!-- <-----------breadcum ----------->

    <!-- <-----------template ----------->
    <section class="template error-page">
      <div class="container text-center">
       <h2 class="f-ex-bold">Oops!</h2>
       <h3 class="f-bold">404 Not Found</h2>
       <p class="f-regular">Sorry, an error has occured, Requested page not found!</p>
       <a href="index.php" class="btns f-bold">Home</a>
       <a href="contact.php" class="btns f-bold">Contact</a>
      </div>
    </section>
    <!-- <-----------template ----------->

   </div>
  <?php include("footer.php"); ?>

</body>
</html> 
