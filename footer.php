<footer>
	<div class="container flex-box">
		<div class="column">
			<img src="images/footer-logo.png" class="img-fluid d-block m-auto" alt="footer-logo">

			<div class="social-box">
				<a href=""><i class="fab fa-facebook-f"></i></a>
				<a href=""><i class="fab fa-linkedin-in"></i></a>
			</div>
		</div>
		<div class="column">
			<h5 class="f-medium white">Services</h5>
			<a href="services.php#freight-forwarding">FREIGHT FORWARDING (AIR & SEA)</a>
			<a href="services.php#custom-clearance">CUSTOM CLEARANCE (AIR & SEA)</a>
			<a href="services.php#transportation">TRASPORTATION</a>
			<a href="services.php#warehouse">WAREHOUSE</a>
			<a href="services.php#project-logistics">PROJECT LOGISTICS</a>
			<a href="services.php#value-added-services">Value Added Services</a>
		</div>
		<div class="column">
			<h5 class="f-medium white">Browse links</h5>
			<a href="services.php#freight-forwording">Home</a>
			<a href="services.php#custom-clearance">About Us</a>
			<a href="services.php#warehouse">Services</a>
			<a href="services.php#transportation">Career</a>
			<a href="services.php#project-logistics">Contact Us</a>
		</div>
		<div class="column">
			<h5 class="f-medium white">Connect us</h5>
			<p class="f-light white">Kukreja Center, Sector No:- 11, CBD Belapur, Navi Mumbai:- 400614.</p>
			<br class="d-none d-lg-block"><br>
			<p class="f-light">Phone: <a href="tel:+91 1234567899">+91 1234567899</a></p>
			<p class="f-light">Email: <a href="mailto:info@dconashippig.com">info@dconashippig.com</a></p>
		</div>
	</div>
	<div class="container btm-strip flex-box">
		<div class="cols">
			<p class="f-light white">Copyright 2020 © Dcon Shipping All rights reserved.</p>
		</div>
		<div class="cols">
			<a href="term-and-condition.php">Terms & Conditions</a>
			<a href="privacy-policy.php">Privacy Policy</a>
			<a href="disclaimer.php">Disclaimer</a>
			<a href="sitemap.php">Sitemap</a>
		</div>
		<div class="cols">
			<p class="f-light white">Designed By <a href="https://www.gordinateur.com/" target="_blank">G-Ordinateur</a></p>
		</div>
	</div>
</footer>
<?php include("script.php"); ?>