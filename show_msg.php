  <?php

    $alert_message=false;
    $sweetalert_css='<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">';
    $sweetalert_js='<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>';
    if(isset($_SESSION['error'])) {
      $alert_message='swal({
        title:"Error",
        text:"'.$_SESSION['error'].'",
        type:"error",
        html:true
      });';

    }
    if(isset($_SESSION['success'])) {
       $alert_message='swal({
        title:"Success",
        text:"'.$_SESSION['success'].'",
        type:"success",
        html:true
      });';
    }

    if($alert_message) {
      echo $sweetalert_js.$sweetalert_css;
      echo '<script type="text/javascript">
        $(document).ready(function() { 
          '.$alert_message.'
        });
        </script>';
    }
	unset($_SESSION['error']);
    unset($_SESSION['success']);
  ?>