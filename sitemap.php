<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
   <title>Sitemap | Dcon Shipping</title>
   <meta name="keywords" content="">
   <meta  name="description" content="">
</head>
<body>
   <?php include("header.php"); ?>
   <div class="main">
   <!-- <-----------breadcum ----------->
     <section class="breadcum line">
      <img src="images/career-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container">
         <div class="caption">
            <h3 class="f-bold white">Sitemap</h3>
         </div>
      </div>
   </section>
   <!-- <-----------breadcum ----------->

    <!-- <-----------template ----------->
    <section class="template sitemap">
      <div class="container">
       <div class="row">
         <div class="col-5 col-sm-4 col-md-3">
           <h5 class="f-bold f-bold">Links</h5>
           <a href="index.php">Home</a>
           <a href="about-us.php">About us</a>
           <a href="services.php">Services</a>
           <a href="career.php">Career</a>
           <a href="contact-us.php">Contact Us</a>
         </div>
         <div class="col-7 col-sm-4 col-md-3">
           <h5 class="f-bold f-bold">Services</h5>
           <a href="services.php#freight-forwarding">Freight Forwarding</a>
           <a href="services.php#custom-clearance">Custom Clearance</a>
           <a href="services.php#transportation">Transportation</a>
           <a href="services.php#warehouse">Warehouse</a>
           <a href="services.php#project-logistics">Project Logistics</a>
           <a href="services.php#value-added-services">Value Aadded Services</a>
         </div>
         <div class="col-6 col-sm-4 col-md-3">
           <h5 class="f-bold f-bold">Other Link</h5>
           <a href="term-and-condition.php">Terms & Conditions</a>
           <a href="privacy-policy.php">Privacy Policy</a>
           <a href="disclaimer.php">Disclaimer</a>
         </div>
       </div>
      </div>
    </section>
    <!-- <-----------template ----------->

   </div>
  <?php include("footer.php"); ?>

</body>
</html> 
