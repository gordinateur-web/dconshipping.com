<?php
	session_start();
	date_default_timezone_set("Asia/Kolkata");
	
	/*
	 * This can be set to anything, but default usage is:
	 *
	 *     development
 	 *     production
	*/
	define('ENVIRONMENT', 'development');
	switch (ENVIRONMENT) {
		case 'development':
			error_reporting(-1);
			ini_set('display_errors', 1);
			break;
		case 'production':
			ini_set('display_errors', 0);
			if (version_compare(PHP_VERSION, '5.3', '>='))
			{
				error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
			}
			else
			{
				error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
			}
		break;

		default:
			header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
			echo 'The application environment is not set correctly.';
			exit(1); // EXIT_ERROR
	}
	require_once("Minifier.php");


	/* Adminpanel details */
	
	define("API_FOLDER","../app/go_api/");
	require_once(API_FOLDER."apiv2.php");
	$auth_array=array(
      //admin login email address
      'admin_email' => 'adminpanel@gordinateur.com',
      //admin login password
      'admin_password' => '2a?"84!?QbqcCB(',
      //client login email address
      'client_email' => 'info@dconashippig.com',
   );


?>