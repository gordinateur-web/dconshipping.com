<?php

	echo generate_script_link([
		'./js/jquery.min.js',
		'./js/bootstrap.min.js',
		'./js/aos.js',
		'./js/parallax.min.js',
		'./js/custom.js',
	])
	
?>