<header class="animated">
  <div class="container">
    <nav class="navbar navbar-expand-md">
      <a class="navbar-brand" href="index.php">
        <img src="images/logo.png" class="img-fluid" alt="Logo">
      </a>
      <button class="navbar-toggler" type="button" id="mobile-menu-action" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar one"></span>
        <span class="icon-bar two"></span>
        <span class="icon-bar three"></span>
      </button>
      <div class="mobile-navbar">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about-us.php">About us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="services.php">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="career.php">career</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact-us.php">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="tel:+911234567899"><span class="icon-phone-call-1 call"></span>+91 1234567899</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>

</header>